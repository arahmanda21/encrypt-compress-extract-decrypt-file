**Praktikum Jaringan Komputer Semester 3**

Aditya Nur Juang Rahmanda / 4210181021

**How to Use It**
1. Open server and client
2. In client, input file's folder location and file's name to be sent
3. Client will automatically encrypt, compress and send the file to server
4. Compressed file will be named as "compressedFile.zip" and encrypted file will be named as "encrypted[filename]"
5. After that, server will receive the file and automatically extract it, and decrypted the extracted file
6. Decrypted file will be named as "decrypted[filename]"