using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransfer
{
    class Server
    {
        static void Main(string[] args)
        {
            try
            {
            	//Membuat sambungan ke client
            	TcpListener tcpListener = new TcpListener(IPAddress.Any, 1234);
            	tcpListener.Start();

            	Console.WriteLine("Server started, waiting for client...");
	
	            while (true)
	            {
	                TcpClient tcpClient = tcpListener.AcceptTcpClient();
	
	                Console.WriteLine("Connected to client");
	
	                StreamReader reader = new StreamReader(tcpClient.GetStream());
	
	                //Menerima Data dari client
	                string fileName = reader.ReadLine();
	                string fileFolder = reader.ReadLine();
	                string pass = reader.ReadLine();
	                string zipName = reader.ReadLine();
	                string cmdFileSize = reader.ReadLine(); 
	                string cmdFileName = reader.ReadLine();
	
	                //Menerima file dari client
	                Console.WriteLine("\nReceiving file...");
	                int length = Convert.ToInt32(cmdFileSize);
	                byte[] buffer = new byte[length];
	                int received = 0;
	                int read = 0;
	                int size = 1024;
	                int remaining = 0;
	
	                while (received < length)
	                {
	                    remaining = length - received;
	                    if (remaining < size)
	                    {
	                        size = remaining;
	                    }
	
	                    read = tcpClient.GetStream().Read(buffer, received, size);
	                    received += read;
	                }
	
	                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
	                {
	                    fStream.Write(buffer, 0, buffer.Length);
	                    fStream.Flush();
	                    fStream.Close();
	                }
	
	                //Menampilkan output bahwa file yang diterima disimpan dalam folder project
	                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);
	
	                int unicode = 92;
	                char character = (char)unicode;
	                string slash = character.ToString();
	
	                //Mengatur letak dimana file zip yang barusan diterima dan letak di mana file akan diekstrak
	                string zipPath = Environment.CurrentDirectory + slash + zipName;
	                string extractPath = fileFolder;
	                
	                //Mengektrak file hasil kompresan berdasarkanletak dimana file zip yang barusan diterima dan letak di mana file akan diekstrak
	                Console.WriteLine("\nExtracting...");
	                ZipFile.ExtractToDirectory(zipPath, extractPath);
	                Console.WriteLine("File extracted successfully in " + extractPath);
	                
	                //Mengatur letak dimana file yang akan didekripsi dan letak di mana hasil file setelah didekripsi
	                string startFilePath = fileFolder + slash + "encrypted" + fileName;
                	string endFilePath = fileFolder + slash + "decrypted" + fileName;
                	
                	//Mendekripsi file berdasarkan letak dimana file yang akan didekripsi, letak di mana hasil file setelah didekripsi, dan passwordnya
                	Console.WriteLine("\nDecrypting...");
                	DecryptFile(startFilePath, endFilePath, pass);
                	Console.WriteLine("File decrypted successfully with pass : " + pass);
	            }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }
        
        //Fungsi untuk Dekripsi File
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }

    }
}
