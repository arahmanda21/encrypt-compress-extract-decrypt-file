using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransfer
{
    class Client
    {
        static void Main(string[] args)
        {
            try
            {
            	//Mengatur letak file yang akan dienkripsi dan dimana hasil enkripsi file dikirim
            	int unicode = 92;
	            char character = (char)unicode;
	            string slash = character.ToString();
	            
            	string fileFolder, fileName;
                
                Console.WriteLine("File Encryption and Compression");
                Console.Write("Input File's Folder Location (full) >> ");
                fileFolder = Console.ReadLine();
                Console.Write("Input File's Name >> ");
                fileName = Console.ReadLine();
                
                string startFilePath = fileFolder + slash + fileName;
                string encryptedFileFolder = fileFolder + slash + "EncryptedFolder";
                
                if (!Directory.Exists(encryptedFileFolder)) {
	                DirectoryInfo di = Directory.CreateDirectory(encryptedFileFolder);
                }
                
                string encryptedFilePath = encryptedFileFolder + slash + "encrypted" + fileName;
                
                //Membuat string berupa password
                Random rnd = new Random();
                int pass = rnd.Next();
                string secretKey = pass.ToString();

                //Mengenkripsi file berdasarkan letak file, letak hasilnya, dan string passwordnya
                Console.WriteLine("\nEncrypting...");
                EncryptFile(startFilePath, encryptedFilePath, secretKey);
                Console.WriteLine("The file encrypted successfully with pass : " + secretKey);
                
                //Mengatur letak file yang akan dikompress dan dimana hasil kompress teks dikirim
                string startZipPath = encryptedFileFolder;
                string zipPath = fileFolder + slash + "compressedFile.zip";
                
                //Mengkompress file berdasarkan letak file dan letak hasilnya
                Console.WriteLine("\nCompressing...");
                ZipFile.CreateFromDirectory(startZipPath, zipPath);
                Console.WriteLine("The file compressed successfully\n");
                
                Directory.Delete(encryptedFileFolder, true);
                
                //Mencoba menyambungkan ke server
                Console.WriteLine("Trying to connect to the server...\n");
                TcpClient tcpClient = new TcpClient("127.0.0.1", 1234);
                Console.WriteLine("Connected. Sending file...");

                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());

                sWriter.WriteLine(fileName); //Mengirim nama file
                sWriter.WriteLine(fileFolder); //Mengirim alamat file folder
                sWriter.WriteLine(secretKey); //Mengirim password ke server
                sWriter.WriteLine("compressedFile.zip"); //Mengirim nama file hasil kompresan ke server
                
                //Mengirim bessarnya ukuran file
                byte[] bytes = File.ReadAllBytes(zipPath);
                sWriter.WriteLine(bytes.Length.ToString());
                sWriter.Flush();

                //Mengirim cmdFileName
                sWriter.WriteLine(zipPath);
                sWriter.Flush();
                
              	//Mengirim file hasil kompresan
                tcpClient.Client.SendFile(zipPath);

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }

        //Fungsi untuk Enkripsi file
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
        }
    }
}
